// Compiled by ClojureScript 0.0-2138
goog.provide('selectorhierarchy.core');
goog.require('cljs.core');
selectorhierarchy.core.get_parent_by_selector = (function get_parent_by_selector(selector){return selector.querySelector().parentNode;
});
selectorhierarchy.core.get_parents_by_selector = (function get_parents_by_selector(selector){var temp__4092__auto__ = selectorhierarchy.core.get_parent_by_selector.call(null,selector);if(cljs.core.truth_(temp__4092__auto__))
{var parent = temp__4092__auto__;return cljs.core.cons.call(null,parent,get_parents_by_selector.call(null,parent));
} else
{return null;
}
});
selectorhierarchy.core.get_map_of_num_parents_by_selector = (function get_map_of_num_parents_by_selector(coll){return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4119__auto__ = (function iter__4752(s__4753){return (new cljs.core.LazySeq(null,(function (){var s__4753__$1 = s__4753;while(true){
var temp__4092__auto__ = cljs.core.seq.call(null,s__4753__$1);if(temp__4092__auto__)
{var s__4753__$2 = temp__4092__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__4753__$2))
{var c__4117__auto__ = cljs.core.chunk_first.call(null,s__4753__$2);var size__4118__auto__ = cljs.core.count.call(null,c__4117__auto__);var b__4755 = cljs.core.chunk_buffer.call(null,size__4118__auto__);if((function (){var i__4754 = 0;while(true){
if((i__4754 < size__4118__auto__))
{var vec__4758 = cljs.core._nth.call(null,c__4117__auto__,i__4754);var v = cljs.core.nth.call(null,vec__4758,0,null);cljs.core.chunk_append.call(null,b__4755,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [v,cljs.core.count.call(null,selectorhierarchy.core.get_parents_by_selector.call(null,v))], null));
{
var G__4760 = (i__4754 + 1);
i__4754 = G__4760;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__4755),iter__4752.call(null,cljs.core.chunk_rest.call(null,s__4753__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__4755),null);
}
} else
{var vec__4759 = cljs.core.first.call(null,s__4753__$2);var v = cljs.core.nth.call(null,vec__4759,0,null);return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [v,cljs.core.count.call(null,selectorhierarchy.core.get_parents_by_selector.call(null,v))], null),iter__4752.call(null,cljs.core.rest.call(null,s__4753__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4119__auto__.call(null,coll);
})());
});
/**
* A function to sort a map by its values
*/
selectorhierarchy.core.sort_map_by_values_desc = (function sort_map_by_values_desc(coll){return cljs.core.into.call(null,cljs.core.sorted_map_by.call(null,(function (k1,k2){return cljs.core.compare.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.get.call(null,coll,k2),k2], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.get.call(null,coll,k1),k1], null));
})),coll);
});
selectorhierarchy.core.get_selector_hierarcy = (function get_selector_hierarcy(selectors){return selectorhierarchy.core.sort_map_by_values_desc.call(null,selectorhierarchy.core.get_map_of_num_parents_by_selector.call(null,selectors));
});
