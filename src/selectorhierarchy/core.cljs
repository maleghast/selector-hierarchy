(ns selectorhierarchy.core)

(defn get-parent-by-selector [selector]
  (.-parentNode (.querySelector selector)))

(defn get-parents-by-selector [selector]
  (when-let [parent (get-parent-by-selector selector)]
    (cons parent (get-parents-by-selector parent))))

(defn get-map-of-num-parents-by-selector [coll]
  (into {} (for [[v] coll] [v (count (get-parents-by-selector v))])))

(defn sort-map-by-values-desc
  "A function to sort a map by its values"
  [coll]
  (into (sorted-map-by
       (fn [k1 k2] (compare
                    [(get coll k2) k2]
                    [(get coll k1) k1])))
  coll))


(defn get-selector-hierarcy
  [selectors]
  (-> selectors
      (get-map-of-num-parents-by-selector)
      (sort-map-by-values-desc)))
